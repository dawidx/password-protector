/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package passwordprotector;

import java.io.*;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import javax.crypto.spec.SecretKeySpec;
import javax.xml.bind.DatatypeConverter;

import javax.crypto.*;

/**
 *
 * @author Dawid
 */
public abstract class Crypto {

    /**
     * @desc Contains absolute path to the file
     */
    public FileOutputStream OutFile;
    public FileInputStream InFile;
    /**
     * @desc Contains password in which the file is encoded
     */
    public String Password;
    /**
     * @desc contains information whether to encode or decode a file
     */
    public int Mode;
    /**
     * @desc Contains the name of the algorithm to be used for the encryption
     */
    public String Algorithm = "DES";
    public String Padding = "DES/ECB/PKCS5Padding";
    /**
     * @desc Cipher is used for
     */
    protected Cipher cipher;

    /**
     *
     * @param path
     * @param password
     */
    public Crypto(String inFilePath, String outFilePath, int mode) throws FileNotFoundException {
        this.init(inFilePath, outFilePath, mode);
    }

    public Crypto(String inFilePath, String outFilePath, String password, int mode) throws FileNotFoundException, NoSuchAlgorithmException, InvalidKeyException, NoSuchPaddingException {
        this.init(inFilePath, outFilePath, mode);
        this.setPassword(password);
    }

    private void init(String inFilePath, String outFilePath, int mode) throws FileNotFoundException {
        this.OutFile = new FileOutputStream(new File(outFilePath));
        this.InFile = new FileInputStream(new File(inFilePath));
        this.Mode = mode;
    }

    public final void setPassword(String password) throws NoSuchAlgorithmException, InvalidKeyException, NoSuchPaddingException {
        this.Password = password;
        cipher = Cipher.getInstance(this.Padding);
        try {
            cipher.init(this.Mode, passwordToBytes(this.Password));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Abstract methods is required for varieties of actions e.g. button press
     */
    public abstract void action();

    /**
     *
     * @todo implement support for 32-bit architecture
     * @return encryption key
     */
    private SecretKeySpec passwordToBytes(String password) throws Exception {
        try {
            //Byte is defined in terms of the computer architecture.
            //In most cases, it will be 64 bits * 8(slots)
            byte[] max = new byte[8];
            //Converting the password to a 64 bit architecture
            byte[] pass = DatatypeConverter.parseBase64Binary(password);
            if (pass.length > max.length) {
                //The size of the password cannot be greater than bit size * slots
                //If it is we simply cut it
                System.arraycopy(pass, 0, max, 0, max.length);
            } else {
                //If it is smaller, we leave the other bits empty
                System.arraycopy(pass, 0, max, 0, pass.length);
            }

            return new SecretKeySpec(max, 0, max.length, this.Algorithm);
        } catch (Exception ex) {
            ex.printStackTrace();
            throw new Exception();
        }
    }

    protected void Encrypt() {
        try (CipherOutputStream cout = new CipherOutputStream(this.OutFile, this.cipher)) {
            byte[] buf = new byte[1024];
            int read = 0;
            while ((read = this.InFile.read(buf)) != -1) //reading data
            {
                cout.write(buf, 0, read);  //writing encrypted data
            }         //closing streams
            this.InFile.close();
            cout.flush();
            cout.close();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    protected void Decrypt() {
        try {
            try (CipherInputStream cin = new CipherInputStream(this.InFile, this.cipher)) {
                byte[] buf = new byte[1024];
                int read = 0;
                while ((read = cin.read(buf)) != -1) //reading encrypted data
                {
                    this.OutFile.write(buf, 0, read);  //writing decrypted data
                }
            }
            this.OutFile.flush();
            this.OutFile.close();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }
}
