/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package passwordprotector;

import java.io.FileNotFoundException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import javax.crypto.Cipher;
import javax.crypto.NoSuchPaddingException;

/**
 *
 * @author Dawid
 */
public class Decrypt extends Crypto {
    
    /**
     * 
     * @param filePath absolute path to the file
     */
    public Decrypt(String filePath, String outputFile) throws NoSuchAlgorithmException, NoSuchPaddingException, InvalidKeyException, FileNotFoundException {
        super(filePath, outputFile, Cipher.ENCRYPT_MODE);
    }
    
    @Override
    public void action() {
        this.Decrypt();
    }
}
