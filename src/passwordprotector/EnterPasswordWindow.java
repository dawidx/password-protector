/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package passwordprotector;
import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.crypto.NoSuchPaddingException;
import javax.swing.*;

/**
 *
 * @author Dawid
 */
public class EnterPasswordWindow extends JFrame implements ActionListener {

    public String password;
    public int type;
    
    JPasswordField passTextBox;
    JLabel resultLabel;
    
    public static int ENCRYPT = 0;
    public static int DECRYPT = 1;
    
    Crypto h;
    
    public EnterPasswordWindow(Crypto h) {
        super("Enter Password");
        this.h = h;
        this.init();
    }
    
    public final void setResultLabel(JLabel result) {
        this.resultLabel = result;
    }
    
    public EnterPasswordWindow() {
        super("Enter Password");
        this.init();
    }
    
    private void init() {
        
        this.passTextBox = new JPasswordField();
        JButton submit = new JButton("OK");
        submit.addActionListener(this);
        this.add(passTextBox, BorderLayout.NORTH);
        this.add(submit, BorderLayout.SOUTH);
        this.setSize(300, 150);
        this.setVisible(true);
        
    }
    
    @Override
    public void actionPerformed(ActionEvent e) {
        try {
            h.setPassword(this.passTextBox.getPassword().toString());
            h.action();
            
            try {
                this.resultLabel.setText("Action Performed Successfuly");
            } catch(NullPointerException ex) {
                System.out.println("Action Successful");
                System.exit(0);
            }
        } catch (NoSuchAlgorithmException ex) {
            Logger.getLogger(EnterPasswordWindow.class.getName()).log(Level.SEVERE, null, ex);
        } catch (InvalidKeyException ex) {
            Logger.getLogger(EnterPasswordWindow.class.getName()).log(Level.SEVERE, null, ex);
        } catch (NoSuchPaddingException ex) {
            Logger.getLogger(EnterPasswordWindow.class.getName()).log(Level.SEVERE, null, ex);
        }
        this.dispose();
    }
    
}
