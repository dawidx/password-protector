/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package passwordprotector;

import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.FileNotFoundException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.crypto.NoSuchPaddingException;
import javax.swing.*;

/**
 *
 * @author Dawid
 */
public class MainWindow extends JFrame implements ActionListener{
    
    JButton encrypt, decrypt;
    JLabel result;
    
    public MainWindow() {
        super("Password Protector");
        this.encrypt = new JButton("Encrypt");
        this.encrypt.addActionListener(this);
        this.add(this.encrypt, BorderLayout.NORTH);
        this.result = new JLabel("");
        this.add(this.result, BorderLayout.CENTER);
        this.decrypt = new JButton("Decrypt");
        this.decrypt.addActionListener(this);
        this.add(this.decrypt, BorderLayout.SOUTH);
        
        this.setSize(300, 300);
    }
    
    @Override
    public void actionPerformed(ActionEvent e) {
        if(e.getSource() == this.encrypt) {
            try {
                this.b1Handler();
            } catch (Exception ex) {
                ex.printStackTrace();
                Logger.getLogger(MainWindow.class.getName()).log(Level.SEVERE, null, ex);
            } 
        } else if (e.getSource() == this.decrypt) {
            try {
                this.b2Handler();
            } catch (NoSuchAlgorithmException ex) {
                Logger.getLogger(MainWindow.class.getName()).log(Level.SEVERE, null, ex);
            } catch (NoSuchPaddingException ex) {
                Logger.getLogger(MainWindow.class.getName()).log(Level.SEVERE, null, ex);
            } catch (InvalidKeyException ex) {
                Logger.getLogger(MainWindow.class.getName()).log(Level.SEVERE, null, ex);
            } catch (FileNotFoundException ex) {
                Logger.getLogger(MainWindow.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }
    
    
    private void b1Handler() throws NoSuchAlgorithmException, NoSuchPaddingException, InvalidKeyException, FileNotFoundException {
        //Chosing the file to be encrypted via JFileChooser
        JFileChooser fileChooser = new JFileChooser();
        //Setting that only one file can be chosen at the time
        fileChooser.setFileSelectionMode(JFileChooser.FILES_ONLY);

        int showOpenDialog = fileChooser.showOpenDialog(this);
        if (showOpenDialog == JFileChooser.APPROVE_OPTION) {
            File f = fileChooser.getSelectedFile();
            EnterPasswordWindow p = new EnterPasswordWindow(new Encrypt(f.getPath(), f.getPath() + ".enc"));
            p.setResultLabel(this.result);
        }
    }
    
    //Decoding
    private void b2Handler() throws NoSuchAlgorithmException, NoSuchPaddingException, InvalidKeyException, FileNotFoundException {
        JFileChooser fileChooser = new JFileChooser();
        fileChooser.setFileSelectionMode(JFileChooser.FILES_ONLY);

        int showOpenDialog = fileChooser.showOpenDialog(this);
        if (showOpenDialog == JFileChooser.APPROVE_OPTION) {
            File f = fileChooser.getSelectedFile();
            EnterPasswordWindow p = new EnterPasswordWindow(new Decrypt(f.getPath(), f.getAbsolutePath().substring(0, f.getAbsolutePath().lastIndexOf("."))));
            p.setResultLabel(this.result);
            
        }
    }
    
}
